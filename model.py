# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()


class Show(Base):
    __tablename__ = 'shows'
    key = Column(String(255), primary_key=True)
    index = Column(String(255))
    addedAt = Column(String(255))
    art = Column(String(255))
    banner = Column(String(255))
    childCount = Column(String(255))
    contentRating = Column(String(255))
    duration = Column(String(255))
    genre = Column(String(255))
    lastViewedAt = Column(String(255))
    leafCount = Column(String(255))
    originallyAvailableAt = Column(String(255))
    rating = Column(String(255))
    ratingKey = Column(String(255))
    role = Column(String(255))
    studio = Column(String(255))
    summary = Column(String(255))
    theme = Column(String(255))
    thumb = Column(String(255))
    title = Column(String(255))
    titleSort = Column(String(255))
    type = Column(String(255))
    updatedAt = Column(String(255))
    viewCount = Column(String(255))
    viewedLeafCount = Column(String(255))
    year = Column(String(255))


class Season(Base):
    __tablename__ = 'seasons'
    key = Column(String(255), primary_key=True)
    index = Column(String(255))
    parentKey = Column(String(255), ForeignKey('shows.key'))
    parentIndex = Column(String(255))
    addedAt = Column(String(255))
    art = Column(String(255))
    lastViewedAt = Column(String(255))
    leafCount = Column(String(255))
    parentRatingKey = Column(String(255))
    parentTheme = Column(String(255))
    parentThumb = Column(String(255))
    parentTitle = Column(String(255))
    ratingKey = Column(String(255))
    summary = Column(String(255))
    thumb = Column(String(255))
    title = Column(String(255))
    type = Column(String(255))
    updatedAt = Column(String(255))
    viewCount = Column(String(255))
    viewedLeafCount = Column(String(255))

    show = relationship(Show)

class Episode(Base):
    __tablename__ = 'episodes'
    key = Column(String(255), primary_key=True)
    index = Column(String(255))
    parentKey = Column(String(255), ForeignKey('seasons.key'))
    parentIndex = Column(String(255))
    grandparentKey = Column(String(255), ForeignKey('shows.key'))
    addedAt = Column(String(255))
    art = Column(String(255))
    contentRating = Column(String(255))
    director = Column(String(255))
    duration = Column(String(255))
    grandparentArt = Column(String(255))
    grandparentRatingKey = Column(String(255))
    grandparentTheme = Column(String(255))
    grandparentThumb = Column(String(255))
    grandparentTitle = Column(String(255))
    lastViewedAt = Column(String(255))
    media = Column(String(255))
    originallyAvailableAt = Column(String(255))
    parentRatingKey = Column(String(255))
    parentThumb = Column(String(255))
    parentTitle = Column(String(255))
    rating = Column(String(255))
    ratingKey = Column(String(255))
    summary = Column(String(255))
    thumb = Column(String(255))
    title = Column(String(255))
    type = Column(String(255))
    updatedAt = Column(String(255))
    viewCount = Column(String(255))
    writer = Column(String(255))
    year = Column(String(255))

    show = relationship(Season)


engine = create_engine(r'sqlite:///db/data')
Session = sessionmaker(bind=engine)
session = Session()

if __name__ == '__main__':
    Base.metadata.create_all(engine)

