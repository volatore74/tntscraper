# -# -*- coding: utf-8 -*-

"""
Module: TNTScraper
Author: Ignazio Ingenito
Created: 2017-01-07
Description: TNTForum Class, manage automatic download of new torrent

Example:, Pool
    To start call the run() method

"""

import re
import sys
import json
import moment
import logging
import datetime
import requests

from bs4 import BeautifulSoup
from itertools import chain
from functools import partial, reduce
from urllib.parse import urljoin, quote
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

from model import *


def get_sections():
    url = u'{url}/library/sections?X-Plex-Token={token}'
    url = url.format(url=TNTScraper.config['plex_host'],
                     token=TNTScraper.config['plex_token'])

    resp = requests.get(url, headers=TNTScraper.headers)
    if resp.status_code != 200:
        return []

    data = json.loads(resp.content)
    sections = [s for s in data['MediaContainer']['Directory'] if s['type'] == 'show']

    pool = ThreadPool(cpu_count())
    data = pool.map(get_shows, sections)
    return data


def get_shows(section):
    url = u'{url}/library/sections/{section}/all?X-Plex-Token={token}'

    key = section['key']
    url = url.format(url=TNTScraper.config['plex_host'],
                     token=TNTScraper.config['plex_token'],
                     section=key)

    resp = requests.get(url, headers=TNTScraper.headers)
    if resp.status_code != 200:
        return []

    data = json.loads(resp.content)
    shows = data['MediaContainer']['Metadata']

    pool = ThreadPool(cpu_count())
    data = pool.map(get_seasons, shows)
    section['shows'] = data

    return section


def get_seasons(show):
    url = u'{url}/library/metadata/{show}/children?X-Plex-Token={token}'
    url = url.format(url=TNTScraper.config['plex_host'],
                     token=TNTScraper.config['plex_token'],
                     show=show['ratingKey'])

    logging.info(u'Loading info {title}...'.format(title=show['title']))
    resp = requests.get(url, headers=TNTScraper.headers)
    if resp.status_code != 200:
        return []

    data = json.loads(resp.content)
    seasons = data['MediaContainer']['Metadata']

    pool = ThreadPool(cpu_count())
    data = pool.map(get_episodes, seasons)
    show['seasons'] = data

    return show


def get_episodes(season):
    url = u'{url}/library/metadata/{season}/children?X-Plex-Token={token}'
    url = url.format(url=TNTScraper.config['plex_host'],
                     token=TNTScraper.config['plex_token'],
                     season=season['ratingKey'])

    resp = requests.get(url, headers=TNTScraper.headers)
    if resp.status_code != 200:
        return []

    data = json.loads(resp.content)
    season['episodes'] = data['MediaContainer']['Metadata']

    return season


class TNTScraper(object):
    config = {
        'tnt_url': u'http://www.tntvillage.scambioetico.org/src/releaselist.php',
        'plex_host': u'http://mediaserver:32400',
        'plex_token': u'BRvLu4pv7q4HdPqqSL1mm',
        'torrent': u'http://mediaserver:8080/gui/',
    }

    db = {}
    plex = None
    token = None
    session = requests.Session()
    headers = {
        u'Accept': 'application/json'
    }

    def get_new_post(self, data):
        url = self.config['tnt_url']
        data = self.session.post(url, data)
        soup = BeautifulSoup(data.content, 'html.parser')
        rows = soup.find_all('tr')
        rows = rows[1:]

        founds = map(self.parse, rows)
        founds = filter(self.filter, founds)
        for file in founds:
            logging.info(f'New episode found {file["title"]}: {file["torrent"]}')
            self.to_webui(file['magnet'])

    def get_new(self):
        data = []
        categories = [1, 29]
        pool = ThreadPool(cpu_count() - 1)

        for c in categories:
            for page in range(10):
                data.append({'cat': c, 'page': page + 1, 'srcrel': u''})

        pool.map(self.get_new_post, data)

    def filter(self, row):
        if not row['has_italian']:
            return False

        shows = [k for k in self.db if self.is_similar(row['title'], k['title'])]

        if not shows:
            return False

        for show in shows:
            try:
                show_seasons = show['seasons']
                row_season = int(row['season'])
                min_episode = int(row['min_episode'])
                max_episode = int(row['max_episode'])
                row_resolution = int(row['resolution'])
                curr_season = max([int(s['index']) for s in show_seasons])

                if row_season > curr_season:
                    logging.info(f'DOWNLOADING: new season detected: {row["title"]}')
                    return True

                episodes = list(chain.from_iterable([e['episodes'] for e in show_seasons if int(e['index']) == row_season]))
                episodes_num = [int(e['index']) for e in episodes]
                episode = [e for e in episodes if int(e['index']) in range(min_episode, max_episode + 1)]

                res = []
                for media in [e['Media'] for e in episode]:
                    for m in media:
                        if 'height' in m.keys():
                            res.append(m['height'])
                ep_resolution = min(res) if res else 0

                if min_episode not in episodes_num and row_season == curr_season:
                    logging.info(f'DOWNLOADING: {row["title"]} new episode detected')
                    return True
                elif max_episode not in episodes_num and row_season == curr_season:
                    logging.info(f'DOWNLOADING: {row["title"]} new episode detected')
                    return True
                elif (float(max(row_resolution, 1)) / float(max(ep_resolution, 1))) > 1.1:
                    logging.info(f'DOWNLOADING: {row["title"]} episode with better resolution '
                                 f'detected ({row_resolution}/{ep_resolution})')
                    return True
            except Exception as e:
                logging.error(f'{show["title"]}: {e.message}')

        return False

    def is_similar(self, a, b):
        a = self.escape(a)
        b = self.escape(b)
        if a in b or b in a:
            return True
        else:
            return False

    def get_plexdb(self):
        sections = get_sections()
        sections = [s['shows'] for s in sections]
        sections = reduce(lambda x, y: x.extend(y), sections)
        self.db = sections
        self.titles = [self.escape(e['title']) for e in self.db]

    def save_db(self):

        for show in self.db:
            new = session.query(Show).get(show['key'])
            if not new:
                new = Show()
                map(lambda attr: setattr(new, attr, show[attr]), [attr for attr in show])
                session.add(new)
            else:
                map(lambda attr: setattr(new, attr, show[attr]), [attr for attr in show])

            for season in show['seasons']:
                new = session.query(Season).get(season['key'])
                if not new:
                    new = Season()
                    map(lambda attr: setattr(new, attr, season[attr]), [attr for attr in season])
                    session.add(new)
                else:
                    map(lambda attr: setattr(new, attr, season[attr]), [attr for attr in season])

                for episode in season['episodes']:
                    new = session.query(Episode).get(episode['key'])
                    if not new:
                        new = Episode()
                        map(lambda attr: setattr(new, attr, episode[attr]), [attr for attr in episode])
                        session.add(new)
                    else:
                        map(lambda attr: setattr(new, attr, episode[attr]), [attr for attr in episode])

        session.commit()

    def download(self, title, season, episode):
        files = self.search(title, season)
        for file in files:
            if int(file['min_episode']) <= episode <= int(file['max_episode']):
                self.to_webui(file['magnet'])

    def to_webui(self, magnet):
        # open URL to get data
        url = u"{url}?action=add-url&s={hash}&token={token}"
        url = url.format(url=self.config['torrent'],
                         hash=quote(magnet),
                         token=self.token)
        res = self.session.get(url, auth=('admin', ''))

        return True if res.status_code == 200 else False

    def search(self, title, season, quality=''):
        found = []
        url = self.config['tnt_url']
        title = u'%s S%02d %s' % (title, season, quality)
        page = 1
        cont = 2

        while True:
            data = self.session.post(url, data={
                # 'cat': 29,
                'page': page,
                'srcrel': title
            })
            soup = BeautifulSoup(data.content, 'html.parser')
            rows = soup.find_all('tr')
            rows = map(self.parse, rows[1:])

            if not rows:
                break

            found.extend(rows)
            page += 1

        return found

    def parse(self, row):
        """
        :type: (Tag, tuple) -> list
        :param row:
        :param show:
        :rtype: list

         row columns
         1 link to torrent file
         2 magnet link
         3 category
         4 leecher
         5 seeder
         6 C
         7 title

        """
        min_episod = None
        max_episod = None
        cols = row.find_all('td')
        texts = cols[6].findAll(text=True)

        try:
            title = texts[0]
            desc = texts[1].replace(u'\xa0', '')

            torrent = cols[0].a['href']
            magnet = cols[1].a['href']

            nosub = desc.lower()
            try:
                nosub = nosub[:nosub.index(' sub')]
            except Exception as e:
                # logging.debug('no subs detected: %r' % (texts,))
                pass

            # detect language
            is_ita = 'ita' in nosub

            # detect encoding
            encdng = re.search('(h264|h265)', nosub, re.IGNORECASE)
            encdng = '' if not encdng else encdng.group()

            # detect resolution
            resolution = re.search('(xvid|720|1080)', desc, re.IGNORECASE)
            resolution = resolution.group() if resolution else 'xvid'
            resolution = resolution.lower().strip()
            resolution = resolution.replace('xvid', '0')
            resolution = int(resolution)

            # detect season
            sesepd = re.search('[S]?(\d+)[e|x](\d+)[-]*(\d+)*', title, re.IGNORECASE)

            if sesepd:
                matched = sesepd.groups()

                try:
                    season = int(matched[0])
                except Exception as e:
                    logging.info(u'error decoding matched series %s\n%r' % (matched[0], e))
                    season = 999

                try:
                    if matched[2]:
                        min_episod = int(matched[1])
                        max_episod = int(matched[2])
                    elif matched[1]:
                        min_episod = int(matched[1])
                        max_episod = int(matched[1])
                except Exception as e:
                    logging.info(u'error decoding matched episodies: %r\n%r' % (matched, e))
                    min_episod = 999
                    max_episod = 999
            else:
                season = 999
                min_episod = 999
                max_episod = 999

            if sesepd:
                position = sesepd.start()
                title = title[:position].strip()

            d = {
                'title': title,
                'season': season,
                'min_episode': min_episod,
                'max_episode': max_episod,
                'description': texts[1],
                'has_italian': is_ita,
                'encoding': encdng,
                'torrent': torrent,
                'magnet': magnet,
                'resolution': resolution,
            }

            return d
        except Exception as e:
            logging.error(u'Error parsing %r\n%r' % (texts[1], e))
            return None


    def fix(self):
        qualities = ['1080 h264', '720 h264', 'h264']
        for show in self.db:
            for season in show['seasons']:
                try:
                    eps = [int(e['index']) for e in season['episodes']]
                    all = range(1, max(eps) + 1)
                    if len(all) != len(eps):
                        logging.info(u'Trying to fix %s Season %02d' % (show['title'], season['index']))
                        missing = set(all) - set(eps)
                        for m in missing:
                            title = show['title'] if show['title'] != u'NCIS - Unità anticrimine' else u'NCIS'
                            title = self.escape(title)
                            for quality in qualities:
                                ret = self.search(self.escape(title), int(season['index']), quality)
                                ret = filter(lambda x: int(x['max_episode']) == m, ret)
                                for r in ret:
                                    self.to_webui(r['magnet'])
                                    logging.info(u'ADDED: {} S{:02d}e{:02d} missing episode'.format(show['title'],
                                                                                                    int(season[
                                                                                                            'index']),
                                                                                                    int(m)))
                                    if r is not None:
                                        break
                                if ret:
                                    break

                except Exception as e:
                    logging.error(u"Can't fix %s season %s: %r", (show['title'], e.message))

    def get_1337x(self):
        headers = {'User-Agent': "It's me"}
        base = 'http://1337x.to'
        url = base + '/category-search/{year}%20{lang}/Movies/{page}/'

        page = 1
        while True:
            www = url.format(base=base, year=datetime.date.today().year, lang='ita', page=page)
            html = requests.get(www, headers=headers)
            soup = BeautifulSoup(html.content, 'html.parser')
            rows = soup.find_all('tr')

            if not rows:
                break

            data = []
            for row in rows:
                cols = row.find_all('td')
                if cols:
                    data.append({
                        'name': row.select('td.name a')[1].text,
                        'href': row.select('td.name a')[1].attrs['href'],
                        'seeds': row.select_one('td.seeds').text,
                        'leeches': row.select_one('td.leeches').text,
                        'date': moment.date(row.select_one('td.coll-date').text).isoformat(),
                        'size': list(row.select_one('td.size').children)[0],
                        'vip': row.select_one('td.vip,td.uploader').text,
                    })

            data = [d for d in data if '1080' in d['name']]
            for d in data:
                url = '{base}{url}'.format(base=base, url=d['href'])
                html = requests.get(url, headers=headers)
                soup = BeautifulSoup(html.content, 'html.parser')
                link = soup.select_one('ul.download-links-dontblock a')
                d['magnet'] = link.attrs['href']

                logging.info('Adding {title}...'.format(title=d['name']))
                self.to_webui(d['magnet'])

            page += 1

    def get_ddunlimited(self):
        with requests.Session() as s:
            url = 'http://ddunlimited.net/ucp.php'
            data = {
                'sid': '36cc9c010fe521b3c885f972efe99026',
                'redirect': 'portal.php',
                'login': 'Login',
                'username': 'dominator',
                'password': 'internazionale',
                # 'autologin': True,
            }
            s.post(url, data=data)

            url = 'http://ddunlimited.net/viewforum.php?f=560'
            r = s.get(url)

            soup = BeautifulSoup(r.content, 'html.parser')
            rows = soup.select('div.forumbg')[-1]
            rows = rows.select('.topics dl')

            # for row in rows:
            #     self.film_decode(s, row)
            pool = ThreadPool(cpu_count()-1)
            film_decode = partial(self.film_decode, s)
            pool.map(film_decode, rows)

    def film_decode(self, session, row):

        check = r'%s' % '|'.join([
            'CAM', 'CAMRip',
            'TS', 'HDTS', 'TELESYNC', 'PDVD', 'PreDVDRip',
            'WP', 'WORKPRINT',
            'TC', 'HDTC', 'TELECINE',
            'SCR', 'SCREENER', 'DVDSCR', 'DVDSCREENER', 'BDSCR',
            'DDC',
            'R5',
            'DSR', 'DSRip', 'SATRip', 'DTHRip', 'DVBRip', 'HDTV', 'PDTV', 'TVRip',
            'WEB(.*)Cap', 'WEBCap', 'WEB Cap', 'WEBRip',
            'MD', '265',
        ])

        cols = row.select('td')
        try:
            title = cols[0].select_one('a').text
            link = cols[0].select_one('a').attrs['href']
            link = urljoin('http://ddunlimited.net', link)
            is_torrent = cols[1].select_one('img[title="BitTorrent"]')
            is_torrent = False if is_torrent is None else True
            props = cols[2].text

            found = re.match(check, props, re.IGNORECASE)
            if found:
                return

            if not is_torrent:
                return

            r = session.get(link)
            soup = BeautifulSoup(r.content, 'html.parser')
            links = soup.select('a.postlink-local')
            links = [r.attrs['href'] for r in links if '.torrent' in r.attrs['href']]

            for link in links:
                logging.info(u'dowloading {title}: {props}'.format(title=title, props=props))
                self.to_webui(link)

        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def escape(text):
        """

        :rtype: str
        """
        text = text.lower()
        text = re.sub(r'\(.*\)', '', text)
        text = re.sub(r' - ', ' ', text)
        text = re.sub(r'the\s+|il\s+|la\s*', ' ', text)
        text = re.sub(r'serie\s*', ' ', text)
        text = re.sub(r'\'s', ' ', text)
        text = re.sub(r'[\.\,\:]', ' ', text)
        text = re.sub(r'\s+', ' ', text)
        text = text.replace('&', 'and')

        match = re.search('[S]?\d+[e|x]\d+[-]*[\d+]*', text, re.IGNORECASE)
        if match:
            match = match.group()
            text = text.replace(match, '')

        text = text.strip()
        text = text.lstrip()
        return text

    def check(self):

        sections = get_sections()
        for s in sections:
            get_seasons()
            logging.debug(s)

    def run(self):
        self.get_plexdb()
        self.save_db()
        self.get_new()
        self.fix()
        # self.get_ddunlimited()
        # self.get_1337x()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    logging.getLogger('chardet').setLevel(logging.ERROR)
    logging.getLogger('urllib3').setLevel(logging.ERROR)
    logging.getLogger('plexapi').setLevel(logging.ERROR)
    logging.getLogger('requests').setLevel(logging.ERROR)

    app = TNTScraper()
    app.run()
