# -*- coding: utf-8 -*-

"""
Module: TNTScraper
Author: Ignazio Ingenito
Created: 2019-01-31
Description: TNTForum Class, manage automatic download of new torrent
"""
import re
import sys
import json
import logging
from itertools import chain
from urllib.parse import quote

import requests

from concurrent.futures import ThreadPoolExecutor
from functools import reduce

from bs4 import BeautifulSoup

from model import session, Show, Season, Episode


class TNTScraper(object):
    config = {
        'tnt_url': u'http://www.tntvillage.scambioetico.org/src/releaselist.php',
        'plex_host': u'http://mediaserver:32400',
        'plex_token': u'BRvLu4pv7q4HdPqqSL1mm',
        'torrent': u'http://mediaserver:8080/gui/',
    }

    db = {}
    plex = None
    token = None
    titles = None
    session = requests.Session()
    headers = {
        u'Accept': 'application/json'
    }

    @staticmethod
    def get_episodes(season):
        key = season['ratingKey']
        url = TNTScraper.config['plex_host']
        token = TNTScraper.config['plex_token']
        url = f'{url}/library/metadata/{key}/children?X-Plex-Token={token}'
        resp = requests.get(url, headers=TNTScraper.headers)
        if resp.status_code != 200:
            return []

        data = json.loads(resp.content)
        season['episodes'] = data['MediaContainer']['Metadata']

        return season

    def get_seasons(self, show):
        key = show['ratingKey']
        url = TNTScraper.config['plex_host']
        token = TNTScraper.config['plex_token']
        url = f'{url}/library/metadata/{key}/children?X-Plex-Token={token}'

        logging.info(f'Loading info {show["title"]}...')
        resp = requests.get(url, headers=TNTScraper.headers)
        if resp.status_code != 200:
            return []

        data = json.loads(resp.content)
        seasons = data['MediaContainer']['Metadata']

        with ThreadPoolExecutor() as pool:
            data = pool.map(self.get_episodes, seasons)
            show['seasons'] = list(data)

        return show

    def get_shows(self, section):
        url = TNTScraper.config['plex_host']
        token = TNTScraper.config['plex_token']
        key = section['key']
        url = f'{url}/library/sections/{key}/all?X-Plex-Token={token}'
        resp = requests.get(url, headers=TNTScraper.headers)
        if resp.status_code != 200:
            return []

        data = json.loads(resp.content)
        shows = data['MediaContainer']['Metadata']

        with ThreadPoolExecutor() as pool:
            data = pool.map(self.get_seasons, shows)
            section['shows'] = list(data)

        return section

    def get_sections(self):
        url = TNTScraper.config['plex_host']
        token = TNTScraper.config['plex_token']
        url = f'{url}/library/sections?X-Plex-Token={token}'
        resp = requests.get(url, headers=TNTScraper.headers)
        if resp.status_code != 200:
            return []

        data = json.loads(resp.content)
        sections = [s for s in data['MediaContainer']['Directory'] if s['type'] == 'show']

        with ThreadPoolExecutor() as pool:
            data = pool.map(self.get_shows, sections)
            data = list(data)
        return data

    @staticmethod
    def escape(text):
        text = text.lower()
        text = re.sub(r'\(.*\)', '', text)
        text = re.sub(r' - ', ' ', text)
        text = re.sub(r'the\s+|il\s+|la\s*', ' ', text)
        text = re.sub(r'serie\s*', ' ', text)
        text = re.sub(r'\'s', ' ', text)
        text = re.sub(r'[\.\,\:]', ' ', text)
        text = re.sub(r'\s+', ' ', text)
        text = text.replace('&', 'and')

        match = re.search('[S]?\d+[e|x]\d+[-]*[\d+]*', text, re.IGNORECASE)
        if match:
            match = match.group()
            text = text.replace(match, '')

        text = text.strip()
        text = text.lstrip()
        return text

    def get_plexdb(self):
        sections = self.get_sections()
        sections = [s['shows'] for s in sections]
        sections = reduce(lambda x, y: x.extend(y), sections)
        self.db = sections
        self.titles = [self.escape(e['title']) for e in self.db]

    def save_db(self):
        for show in self.db:
            new = session.query(Show).get(show['key'])
            if not new:
                new = Show()
                for attr in show:
                    setattr(new, attr, show[attr])
                session.add(new)
            else:
                for attr in show:
                    setattr(new, attr, show[attr])

            for season in show['seasons']:
                new = session.query(Season).get(season['key'])
                if not new:
                    new = Season()
                    for attr in season:
                        setattr(new, attr, season[attr])
                    session.add(new)
                else:
                    for attr in season:
                        setattr(new, attr, season[attr])

                for episode in season['episodes']:
                    new = session.query(Episode).get(episode['key'])
                    if not new:
                        new = Episode()
                        for attr in episode:
                            setattr(new, attr, episode[attr])
                        session.add(new)
                    else:
                        for attr in episode:
                            setattr(new, attr, episode[attr])

        session.commit()

    @staticmethod
    def parse(row):
        """
        :type: (Tag, tuple) -> list
        :param row:
        :param show:
        :rtype: list

         row columns
         1 link to torrent file
         2 magnet link
         3 category
         4 leecher
         5 seeder
         6 C
         7 title

        """
        min_episode = None
        max_episode = None
        cols = row.find_all('td')
        texts = cols[6].findAll(text=True)

        try:
            title = texts[0]
            desc = texts[1].replace(u'\xa0', '')

            torrent = cols[0].a['href']
            magnet = cols[1].a['href']

            no_sub = desc.lower()
            try:
                no_sub = no_sub[:no_sub.index(' sub')]
            except Exception as e:
                pass

            # detect language
            is_ita = 'ita' in no_sub

            # detect encoding
            encoding = re.search('(h264|h265)', no_sub, re.IGNORECASE)
            encoding = '' if not encoding else encoding.group()

            # detect resolution
            resolution = re.search('(xvid|720|1080)', desc, re.IGNORECASE)
            resolution = resolution.group() if resolution else 'xvid'
            resolution = resolution.lower().strip()
            resolution = resolution.replace('xvid', '0')
            resolution = int(resolution)

            # detect season
            session_ep = re.search(r'[S]?(\d+)[e|x](\d+)[-]*(\d+)*', title, re.IGNORECASE)

            if session_ep:
                matched = session_ep.groups()

                try:
                    season = int(matched[0])
                except Exception as e:
                    logging.info(f'error decoding matched series {matched[0]}\n{str(e)}')
                    season = 999

                try:
                    if matched[2]:
                        min_episode = int(matched[1])
                        max_episode = int(matched[2])
                    elif matched[1]:
                        min_episode = int(matched[1])
                        max_episode = int(matched[1])
                except Exception as e:
                    logging.info(f'error decoding matched episodes: {matched}\n{str(e)}')
                    min_episode = 999
                    max_episode = 999
            else:
                season = 999
                min_episode = 999
                max_episode = 999

            if session_ep:
                position = session_ep.start()
                title = title[:position].strip()

            return {
                'title': title,
                'season': season,
                'min_episode': min_episode,
                'max_episode': max_episode,
                'description': texts[1],
                'has_italian': is_ita,
                'encoding': encoding,
                'torrent': torrent,
                'magnet': magnet,
                'resolution': resolution,
            }
        except Exception as e:
            logging.error(f'Error parsing {texts[1]}\n{str(e)}')
            return None

    def filter(self, row):
        if not row['has_italian']:
            return False

        shows = [k for k in self.db if self.is_similar(row['title'], k['title'])]
        if not shows:
            return False

        for show in shows:
            try:
                show_seasons = show['seasons']
                row_season = int(row['season'])
                min_episode = int(row['min_episode'])
                max_episode = int(row['max_episode'])
                row_resolution = int(row['resolution'])
                curr_season = max([int(s['index']) for s in show_seasons])

                if row_season > curr_season:
                    logging.info(f'DOWNLOADING: new season detected: {row["title"]}')
                    return True

                episodes = list(chain.from_iterable([e['episodes'] for e in show_seasons if int(e['index']) == row_season]))
                episodes_num = [int(e['index']) for e in episodes]
                episode = [e for e in episodes if int(e['index']) in range(min_episode, max_episode + 1)]

                res = []
                for media in [e['Media'] for e in episode]:
                    for m in media:
                        if 'height' in m.keys():
                            res.append(m['height'])
                ep_resolution = min(res) if res else 0

                if min_episode not in episodes_num and row_season == curr_season:
                    logging.info(f'DOWNLOADING: {row["title"]} new episode detected')
                    return True
                elif max_episode not in episodes_num and row_season == curr_season:
                    logging.info(f'DOWNLOADING: {row["title"]} new episode detected')
                    return True
                elif (float(max(row_resolution, 1)) / float(max(ep_resolution, 1))) > 1.1:
                    logging.info(f'DOWNLOADING: {row["title"]} episode with better resolution '
                                 f'detected ({row_resolution}/{ep_resolution})')
                    return True
            except Exception as e:
                logging.error(f'{show["title"]}: {e.message}')

        return False

    def to_webui(self, magnet):
        # open URL to get data
        url = self.config['torrent'],
        hash = quote(magnet),
        token = self.token
        url = f"{url}?action=add-url&s={hash}&token={token}"
        res = self.session.get(url, auth=('admin', ''))
        return True if res.status_code == 200 else False

    def get_new_post(self, data):
        url = self.config['tnt_url']
        logging.info(url)
        data = self.session.post(url, data)
        soup = BeautifulSoup(data.content, 'html.parser')
        rows = soup.find_all('tr')
        rows = rows[1:]

        logging.info(f'get_new_post: {rows}')
        with ThreadPoolExecutor() as pool:
            founds = pool.map(self.parse, rows)
            founds = filter(self.filter, founds)

        for file in founds:
            logging.info(f'New episode found {file["title"]}: {file["torrent"]}')
            self.to_webui(file['magnet'])

    def get_new(self):
        data = []
        categories = [1, 29]

        for c in categories:
            for page in range(10):
                data.append({'cat': c, 'page': page + 1, 'srcrel': u''})

        with ThreadPoolExecutor() as pool:
            pool.map(self.get_new_post, data)

    def fix(self):
        qualities = ['1080 h264', '720 h264', 'h264']
        for show in self.db:
            for season in show['seasons']:
                try:
                    eps = [int(e['index']) for e in season['episodes']]
                    all = range(1, max(eps) + 1)
                    if len(all) != len(eps):
                        logging.info(u'Trying to fix %s Season %02d' % (show['title'], season['index']))
                        missing = set(all) - set(eps)
                        for m in missing:
                            title = show['title'] if show['title'] != u'NCIS - Unità anticrimine' else u'NCIS'
                            title = self.escape(title)
                            for quality in qualities:
                                ret = self.search(self.escape(title), int(season['index']), quality)
                                ret = filter(lambda x: int(x['max_episode']) == m, ret)
                                for r in ret:
                                    self.to_webui(r['magnet'])

                                    _title = show['title']
                                    _season = int(season['index'])
                                    _episode = int(m)
                                    logging.info(f'ADDED: {_title} S{_season:02d}e{_episode:02d} missing episode')
                                    if r is not None:
                                        break
                                if ret:
                                    break

                except Exception as e:
                    _title = show['title']
                    logging.error(f"Can't fix {_title} season {season['index']}: {str(e)}")

    @classmethod
    def run(cls):
        obj = cls()
        obj.get_plexdb()
        obj.save_db()
        obj.get_new()
        obj.fix()
        # self.get_ddunlimited()
        # self.get_1337x()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    logging.getLogger('chardet').setLevel(logging.ERROR)
    logging.getLogger('urllib3').setLevel(logging.ERROR)
    logging.getLogger('plexapi').setLevel(logging.ERROR)
    logging.getLogger('requests').setLevel(logging.ERROR)

    TNTScraper.run()
